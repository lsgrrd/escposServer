const express = require('express')
const cors = require('cors')
const printer = require('./printer.js')

const app = express()
const port = 3000
app.use(cors())
app.use(express.json())

app.post('/', (req, res) => {
  printer.print(req.body.text)
    .then(response => {
      res.status(200).send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/ticket', (req, res) => {
  printer.ticket(req.body.order)
    .then(response => {
      res.send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.post('/open_cd', (req, res) => {
  printer.openCd(req.body.order)
    .then(response => {
      res.send({
        status: response
      })
    })
    .catch(err => {
      console.log(err)
    })
})

app.listen(port, () => {
  console.log(`🖨 Printer listening at http://localhost:${port}`)
})