const escpos = require('escpos')
escpos.USB = require('escpos-usb')
const path = require('path');
var dayjs = require('dayjs')


const device  = new escpos.USB()

const options = {
  width: 42,
}

const printer = new escpos.Printer(device, options);

function print(message) {
  return new Promise((resolve, reject) => {
    device.open(function(error){
      printer
        .font('b')
        .align('lt')
        .style('normal')
        .size(0.5, 0.5)
        .text(message)
        .drawLine()
        .tableCustom(
          [
            { text:"Left", align:"LEFT", width:0.8 },
            { text:"Right", align:"RIGHT", width:0.2 }
          ]
        )
        .size(1, 1)
        .qrimage(message, function(err){
          this.close()
        })

      resolve('ok')
    })
  })
}

function ticket(order) {
  return new Promise((resolve) => {
    device.open(function () {
      printer
        .font('b')
        .align('ct')
        .size(1,1)
        .text(order.store)
        .size(0.5, 0.5)
        .text(dayjs(order.date).format('DD/MMM/YY HH:mm'))
        .align('lt')
        .size(0.5, 0.5)
        .drawLine()
      order.products.forEach(product => {
        printer
          .tableCustom(
            [
              { text: `${product.units} - ${product.name} ${product.variant}`, align:"LEFT", width: 0.7 },
              { text: `$${product.unit_price}`,align:"RIGHT", width: 0.3 },
            ]
          )
      })
      printer
        .drawLine()
        .tableCustom(
          [
            { text: 'Subtotal', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.subtotal.toFixed(2)}`, align:"RIGHT", width:0.3},
          ]
        )
        .tableCustom(
          [
            { text: 'IVA', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.iva.toFixed(2)}`, align:"RIGHT", width:0.3},
          ]
        )
        .tableCustom(
          [
            { text: 'Total', align:"LEFT", width:0.7, style: 'B' },
            { text: `$${order.total.toFixed(2)}`, align:"RIGHT", width:0.3},
          ]
        )
        .control('LF')
        .align('ct')
        .text('GRACIAS')
        .cashdraw(5)
        .cashdraw(2)
        .control('LF')
        .control('LF')
        .control('LF')
        .close()

    })
    resolve()
  })
}

function openCd(message) {
  return new Promise((resolve, reject) => {
    device.open(function(error){
      printer
        .cashdraw(2)
        .close()
      resolve('ok')
    })
  })
}



// exports.print = print
module.exports = {
  print,
  ticket,
  openCd,
}